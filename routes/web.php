<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ApprenantController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Route::view('add', 'apprenants.add');
// Route::view('/list', 'apprenants.list');

/**APPRENANT CRUD */
// Creer un apprenant
Route::get('/apprenant/list/', [ApprenantController::class,'index']);
Route::post('/apprenant/create/', [ApprenantController::class,'store']);
Route::get('/apprenant/detail/{id}', [ApprenantController::class,'show']);
Route::get('/apprenant/edit/{id}', [ApprenantController::class,'edit']);
Route::post('/apprenant/update/{id}', [ApprenantController::class,'update']) ;
// Route::post('/apprenant/create/', 'ApprenantController@store');



