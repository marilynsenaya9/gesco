<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Apprenant;

class ApprenantController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $apprenants = Apprenant::all();


        return view('apprenants.list', compact([
            'apprenants',
        ]));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $r)
    {
        //
        // $data = ();

        $newApp = Apprenant::create([
            'contact' => $r->contact,
            'nom' => $r->nom,
            'prenom' => $r->prenom,
            'adresse' => $r->adresse,
            'matricule' => $r->matricule,
            'mail' => $r->email,
            'dateNaissance' => $r->date_nais,
            'genre' => $r->genre,
        ]);

        //store pic

        return redirect('/apprenant/list');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $apprenant = Apprenant::find($id);

        return view( 'apprenants.detail', compact([
            'apprenant',
        ]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $old = Apprenant::find($id);

        return view( 'apprenants.modifier', compact([
            'old',
        ]));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $r, $id)
    {
        //
        $data = ([
            'contact' => $r->contact,
            'nom' => $r->nom,
            'prenom' => $r->prenom,
            'adresse' => $r->adresse,
            'matricule' => $r->matricule,
            'mail' => $r->email,
            'dateNaissance' => $r->date_nais,
            'genre' => $r->genre,
        ]);

        $old = Apprenant::find($id);
        $new = $old->update($data);

        return redirect('/apprenant/list');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
