@extends('index')
@section('content')
<p class="d-none">
    {{$titre = 'liste des apprenants'}}
</p>
<div class="pcoded-inner-content">
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h5 class="m-0 font-weight-bold text-primary text-center">Listes des apprenants</h5>
    </div>
  
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr class="text-center">
                        <th>Nom & Prénoms</th>
                        <th>Sexe</th>
                        <th>Niveau</th>
                        <th >Actions</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr class="text-center">
                        <th>Nom & Prénoms</th>
                        <th>Sexe</th>
                        <th>Niveau</th>
                        <th >Actions</th>
                    </tr>
                </tfoot>
                <tbody>
                    @foreach( $apprenants as $a)
                        <tr>
                            <td> {{ $a['nom'] }} </td>
                            <td>{{ $a['genre']}}</td>
                            <td>{{ $a['niveau'] }}</td>
                            <td class="text-center">
                                <a href="/apprenant/detail/{{ $a['id'] }}"> 
                                <button class="btn btn-primary waves-effect waves-light mx-auto" data-toggle="tooltip" data-placement="bottom" title="Voir  détails sur l'apprenant">
                                    <i class="fa fa-pencil  float-right text-black" > </i>
                                    </button>
                                </a>
                                <a href="" data-toggle="modal" data-target="#exampleModal">
                                    <button class="btn btn-success waves-effect waves-light mx-auto" data-toggle="tooltip" data-placement="bottom" title="Imprimer la carte">
                                    <i class="fa fa-id-card float-right text-black" > </i>
                                    </button>
                                </a>
                                <a href="" data-toggle="modal" data-target="#exampleModal1">
                                    <button class="btn btn-info waves-effect waves-light mx-auto" data-toggle="tooltip" data-placement="bottom" title="Imprimer attestation">
                                    <i class="fa fa-print  float-right text-black" > </i>
                                    </button>
                                </a>
                            </td>
                            
                        </tr>               
                    @endforeach
                     
                </tbody>
            </table>
        </div>
    </div>
</div>             
</div>
@endsection
<!-- modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">

<div class="modal-dialog ">
      <div class="modal-content">
        <div >
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
        <div class="row text-center">
            <div class="col ">
                <button class="btn btn-primary"> Imprimer la carte
                   <!-- <i class="fa fa-pencil  float-right text-black" > </i> -->
                </button>
            </div>
        </div>

        
        <div class="row ">
            <div class="col-sm-6">
                <img src="/img/profil2.jpg" class="profil" style="height : 75%">
            </div>
            <div class="col-sm-6 mt-3">
                <div class="row">Nom & prenoms</div>
                <div class="row">Date de naissance</div>
                <div class="row">Niveau</div>
                <div class="row">Contact</div>
                <div class="row">contact parent</div>
            </div>
        </div>
        <div class="row " style="margin-top : -4em">
            <div class="col text-center"> 
                <img class="img-fluid" src="/img/gesco2.jpg" alt="Theme-Logo" style="height: 40px; width : 100px"/>
            </div>    
        </div>
        <div class="row " style="margin-top : -1em">
            <div class="col text-center"> <i class="text-center">Devise de l'école</i> </div>    
        </div>

        </div>
      </div>
    </div>
  </div>