@extends('index')
@section('content')
<p class="d-none">
    {{ $titre = 'Ajouter un apprenant'} }
</p>
<div class="container">
   
    <div class="card shadow mb-5 col-10 centrage " style="margin-top : 6em">
        <div class=" py-3">
            <h5 class="m-0 font-weight-bold text-primary text-center" >Ajouter un apprenant</h5>
        </div>
      
        <div class="card-body">
            <form class="user mt-1" method="POST" action="/apprenant/create" enctype="multipart/form-data">
                @csrf
                        
                <div class="form-group row">
                    <div class="col-sm-6 mb-3 mb-sm-0">
                        <label for=""> Nom</label>
                        <input type="text" class="form-control form-control-user" id=""
                            name="nom" >
                    </div>
                    <div class="col-sm-6">
                        <label for=""> Prénoms</label>
                        <input type="text" class="form-control form-control-user" id=" "
                                name="prenom" >
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-6 mb-3 mb-sm-0">
                        <label for=""> Niveau</label>
                        <input type="text" class="form-control form-control-user" id=" "
                            name="niveau" >
                    </div>
                    <div class="col-sm-6">
                        <label for="">Contact</label>
                        <input type="text" class="form-control form-control-user" id="exampleInputEmail"
                        name="contact" >
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-6 mb-3 mb-sm-0">
                        <label for=""> Adresse</label>
                        <input type="text" class="form-control form-control-user" id=" "
                            name="adresse" required >
                    </div>
                    <div class="col-sm-6">
                        <label for=""> Adresse mail</label>
                        <input type="email" class="form-control form-control-user" id="exampleInputEmail"
                        name="email" required >
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-6 mb-3 mb-sm-0 ">
                        <label for=""> Contact parent ou tuteur</label>
                        <input type="tel" class="form-control form-control-user"
                            id="" name="telephone" >
                    </div>
                    <div class="col-sm-6 mb-3 mb-sm-0 ">
                        <label for=""> Date de naissance</label>
                        <input type="date" class="form-control form-control-user"
                            id=""  name="date_nais" >
                    </div>
                </div>
                <div class="form-group row">
                        <div class="col-sm-6 mb-3 mb-sm-0 ">
                        <label for=""> Photo</label>
                            <input type="file" class="form-control form-control-user"id="" name="photo" >
                        </div>
                        <div class="col-sm-6 mb-3 mb-sm-0 ">
                            <label for=""> Sexe</label> <br>
                                <div class="form-check-inline mt-2">
                                <label class="form-check-label" for="genre">
                                    <input type="radio" class="form-check-input" id="radio1" name="genre" value="F" checked>Feminin
                                    </label>
                            </div>
                                <div class="form-check-inline mt-2">
                                <label class="form-check-label" for="genre">
                                    <input type="radio" class="form-check-input" id="radio2" name="genre" value="M">Masculin
                                </label>
                                </div>
                            </div>
                    </div>          
                
                
                <div class="form-group row justify-content-center mx-auto mt-3">
                        <div class=" mx-4 mb-3 mb-sm-0">
                            <button class="btn btn-primary" type="submit" >Enregistrer</button>
                        </div>
                        <div class="">
                            <button class="btn btn-secondary" type="reset" > &nbsp Annuler &nbsp</button>
                        </div>
                </div>
                   
            </form>
        </div>
    </div> 
</div>
@endsection