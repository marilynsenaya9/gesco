@extends('index')
@section('content')
<p class="d-none">
    {{$titre = 'Modifier les info de '}}
</p>
<div class="container">
   
    <div class="card shadow mb-5 col-10 centrage " style="margin-top : 6em">
        <div class=" py-3">
            <h5 class="m-0 font-weight-b$$old text-primary text-center" >Modifier l'apprenant</h5>
        </div>
    @isset($old)
        <div class="card-body">
            <form class="user mt-2" method="POST" action="/apprenant/update/{{ $old['id']}}" enctype="multipart/form-data">
                @csrf
                <div  class="row" style="">
                    <p class="centrage" style="">
                        <img src="/img/profil2.jpg" class="profil" >
                    </p>    
                </div>
                <div class="row mb-4">
                    <div class="centrage">
                        <i class="fa fa-camera text-primary" aria-hidden="true"></i> &nbsp
                        <a href="#">
                            Modifier
                        </a>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-6 mb-3 mb-sm-0">
                        <label for=""> Nom</label>
                        <input type="text" class="form-control form-control-user" id="exampleFirstName"
                            name="nom"  value="{{ $old['nom']}}">
                    </div>
                    <div class="col-sm-6">
                        <label for=""> Prénoms</label>
                        <input type="text" class="form-control form-control-user" id="exampleLastName"
                                name="prenom" value="{{ $old['prenom']}}" >
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-6 mb-3 mb-sm-0">
                        <label for=""> Niveau</label>
                        <input type="text" class="form-control form-control-user" id="exampleLastName"
                            name="niveau" >
                    </div>
                    <div class="col-sm-6">
                        <label for="">Contact</label>
                        <input type="text" class="form-control form-control-user" id="exampleInputEmail"
                        name="contact"  value="{{ $old['contact']}}">
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-6 mb-3 mb-sm-0">
                        <label for=""> Adresse</label>
                        <input type="text" class="form-control form-control-user" id="exampleLastName"
                            name="adresse" value="{{ $old['adresse']}}" >
                    </div>
                    <div class="col-sm-6">
                        <label for=""> Adresse mail</label>
                        <input type="email" class="form-control form-control-user" id="exampleInputEmail"
                        name="email" value="{{ $old['mail']}}" >
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-6 mb-3 mb-sm-0 ">
                        <label for=""> Contact parent ou tuteur</label>
                        <input type="tel" class="form-control form-control-user"
                            id="" name="telephone" value=" " >
                    </div>
                    <div class="col-sm-6 mb-3 mb-sm-0 ">
                        <label for=""> Date de naissance</label>
                        <input type="date" class="form-control form-control-user"
                            id=""  name="date_nais" value="{{ $old['dateNaissance']}}">
                    </div>
                </div>
                <div class="form-group row">
                        
                        <div class="col-sm-6 mb-3 mb-sm-0 ">
                            <label for=""> Sexe</label> <br>
                                <div class="form-check-inline mt-2">
                                <label class="form-check-label" for="radio1">
                                    <input type="radio" class="form-check-input" id="radio1" name="optradio" value="option1" checked>Feminin
                                    </label>
                            </div>
                                <div class="form-check-inline mt-2">
                                <label class="form-check-label" for="radio2">
                                    <input type="radio" class="form-check-input" id="radio2" name="optradio" value="option2">Masculin
                                </label>
                                </div>
                        </div>
                        <div class="col-sm-6 mb-3 mb-sm-0 ">
                        </div>
                    </div>          
                
                
                <div class="form-group row justify-content-center mx-auto mt-2">
                        <div class=" mx-4 mb-3 mb-sm-0">
                            <button class="btn btn-primary" type="submit" >Enregistrer</button>
                        </div>
                        <div class="">
                            <button class="btn btn-secondary" type="reset" > &nbsp Annuler &nbsp</button>
                        </div>
                </div>
                   
            </form>
        </div>
    @endisset
    </div> 
</div>
@endsection