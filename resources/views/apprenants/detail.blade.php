@extends('index')
@section('content')
<p class="d-none">
    {{$titre = 'detail les info de '}}
</p>
<div class="container">
   
<div class="card shadow mb-5 col-10 " style="display: block;margin-left: auto;margin-right: auto ">
        <div class=" py-3">
            <h5 class="m-0 font-weight-bold text-primary text-center">Détail sur l'apprenant</h5>
        </div>
      
        <div class="card-body">
          @isset ($apprenant)
            <table class="table table-striped justify-content-center">
                
                <tbody>
                  <tr class="row">
                    <td class="centrage" >
                        <img src="/img/profil2.jpg" class="profil" >
                    </td>
                  </tr>
                  <tr class="row" >
                    <td class="col-6 large" > <strong>Nom</strong> : {{$apprenant['nom']}} </td>
                    <td class="col-6 large" > <strong>Prénoms</strong> : {{$apprenant['prenom']}}</td>
                  </tr>
                  <tr class="row" >
                    <td class="col-6 large"> <strong>Date de naissance</strong> : {{$apprenant['dateNaissance']}}</td>
                    <td class="col-6 large"> <strong>Téléphone</strong> : {{$apprenant['contact']}}</td>
                  </tr>
                  <tr class="row">
                    <td class="col-6 large"> <strong>Adresse</strong>  : {{$apprenant['adresse']}}</td>
                    <td class="col-6 large"> <strong>Adresse mail</strong>  : {{$apprenant['mail']}} </td>
                  </tr>
                  <tr class="row">
                    <td class="col-6 large"> <strong>Contact parent ou tuteur</strong>  :  - </td>
                    <td class="col-6 large"> <strong>Niveau</strong>  :  - </td>
                  </tr>
                  <tr class="row">
                      <td class="col-12 mt-2">
                          <a href="/apprenant/edit/{{ $apprenant['id']}}">
                            <button class="btn btn-primary float-right mt-2 mr-3" >Modifier </button>
                          </a>
                     </td>
                  </tr>
                </tbody>
                
            </table>
          @endisset  
        </div>
    </div> 
</div>
@endsection